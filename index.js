process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const express = require('express')
const bodyParser = require('body-parser')
const { connection, validateRequirements, makeRequest, pad, errorCodes } = require('./tools')

const port = 3001
const app = express()
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.post('/register', validateRequirements, async (req, res) => {
  // obtiene la informacion de la tarjeta a guardar
  const { cardName, cardNumber, expiration, security } = req.cardInfo

  try {
    const knex = await require('knex')({ client: 'pg', connection })

    // inserta en la base de datos
    const registration = await knex('registrations')
      .returning('id')
      .insert({
        noc: cardName,
        cno: cardNumber,
        exp: expiration,
        ver: security
      })

    await knex.destroy()
    // retorna el id de la tarjeta
    return res.send(registration[0]);

  } catch (error) {
    console.error(error)
    return res.status(500).send('Error trying to register the card in database');
  }
});

app.post('/requestPayment', async (req, res) => {
  const { cardId, amount } = req.body

  const knex = await require('knex')({ client: 'pg', connection })
  const dailyAttempt = await knex('requests').max('id as id').first()
  const audit = pad(dailyAttempt.id + 1, 6) // Genera un numero de auditoria
  const receipt = pad(dailyAttempt.id + 1, 6) // Genera un numero de recibo

  const card = await knex('registrations').where({ id: cardId }).first()

  try {

    // Verifica que exista el cardid

    if (!card) {
      return res.status(400).send('Id de tarjeta no existe');
    }

    // inserta attempt en base de datos
    await knex('requests').insert({ cardId, amount, audit, receipt })

    const {
      cliente_trans_referencia,
      cliente_trans_autoriza,
      cliente_trans_respuesta
    } = await makeRequest(card.cno, amount, card.exp, card.ver, audit, receipt) // realiza la solicitud y obtiene las respuestas

    // Actualiza el campo de la base de datos 
    await knex('requests')
      .where('audit', audit)
      .update({
        res_referencia: cliente_trans_referencia,
        res_autoriza: cliente_trans_autoriza,
        res_respuesta: cliente_trans_respuesta
      })

    await knex.destroy()

    if (cliente_trans_respuesta == '00') {
      return res.send('Transaccion completada');
    } else {
      const error = errorCodes(cliente_trans_respuesta)
      return res.status(500).send(`Ha ocurrido un error con la transacción del tipo ${error}`)
    }
  } catch (error) {
    await knex.destroy()
    console.error(error)
    const response = 'Ocurrió un error con la transacción.'
    try {
      await makeRequest(card.cno, amount, card.exp, card.ver, audit, receipt, 'reverse')
      return res.status(400).send(`${response} Se ha cancelado la solicitud con el procesador de pagos.`)
    } catch (error) {
      console.error(error)
      return res.status(400).send(`${response} ${error.message}`)
    }
  }
})

app.listen(port, () => console.info(`Server listening on port ${port}!`))