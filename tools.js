const soapRequest = require('easy-soap-request');
const parser = require('xml2json');

const comid = 'SERVUNITYT'
const comkey = '$erfins@'
const comwrkstation = 'WORKSUNITYT'
const TERMINALID = '00299997'
const RETAILERID = '000999999999999'
const conf = {
  url: 'https://pgtest.redserfinsa.com:2027/WebPubTransactor/TransactorWS?WSDL',
  headers: {
    'Content-Type': 'text/xml;charset=UTF-8',
  },
  timeout: 35000
}

function pad(n, width) {
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
}

function parseAmount(amount) {
  return pad(parseFloat(amount).toFixed(2).replace('.', ''), 12)
}

function digestData(response) {
  return parser.toJson(response.body, { object: true })['S:Envelope']['S:Body']['ns2:cardtransactionResponse'].return
}

const makeRequest = async function (card, amount, expiration, security, audit, receipt, type = 'make') {
  const xml = `
  <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservices.serfinsa.sysdots.com/">
    <soapenv:Header/>
    <soapenv:Body>
      <web:cardtransaction>
        <security>{"comid":"${comid}","comkey":"${comkey}","comwrkstation":"${comwrkstation}"}</security>
        <txn>${type == 'make' ? 'MANCOMPRANOR' : 'MANCOMPRANORR'}</txn>
        <message>{
          "CLIENTE_TRANS_TARJETAMAN":"${card}",
          "CLIENTE_TRANS_MONTO":"${parseAmount(amount)}",
          "CLIENTE_TRANS_AUDITNO":"${audit}",
          "CLIENTE_TRANS_TARJETAVEN":"${expiration.split('/').reverse().join('')}",
          "CLIENTE_TRANS_MODOENTRA":"012",
          "CLIENTE_TRANS_TERMINALID":"${TERMINALID}",
          "CLIENTE_TRANS_RETAILERID":"${RETAILERID}",
          "CLIENTE_TRANS_RECIBOID":"${receipt}",
          "CLIENTE_TRANS_TOKENCVV":"1611 ${security}"
        }
        </message>
      </web:cardtransaction>
    </soapenv:Body>
  </soapenv:Envelope>`
  const { response } = await soapRequest({ ...conf, xml });
  return JSON.parse(digestData(response))
}

const validateRequirements = function (req, res, next) {
  const { cardName, cardNumber, expiration, security } = req.body

  // verifica la tarjeta a usar
  if (parseInt(cardNumber).toString().length != 16) {
    return res.status(500).send('Tarjeta invalida, deben ser 16 digitos sin guiones medios.');
  }

  // valida cvv
  const check = security.split('').filter(number => !isNaN(number))
  if (security.length != 3 || check.length != 3) {
    return res.status(500).send('CVV incorrecto, deben ser 3 digitos.');
  }

  // Verifica la fecha de expiracion

  req.cardInfo = { cardName, cardNumber, expiration, security }
  next()
}

module.exports = {
  connection: {
    host: "192.168.101.102",
    user: 'postgres',
    password: 'cards_password',
    database: 'postgres',
    port: 6000,
  },
  validateRequirements,
  pad,
  errorCodes(code) {
    const codes = {
      '00': "AUTORIZADO",
      '01': "LLAMAR AL EMISOR",
      '02': "LLAMAR AL EMISOR",
      '03': "LLAMAR AL EMISOR",
      '04': "TARJETA BLOQUEADA",
      '05': "LLAMAR AL EMISOR",
      '07': "TARJETA BLOQUEADA",
      '12': "TRANSACCION INVALIDA",
      '13': "MONTO INVALIDO",
      '14': "LLAMAR AL EMISOR",
      '15': "EMISOR NO DISPONIBLE",
      '19': "REINTENTE TRANSACCION",
      '25': "LLAMAR AL EMISOR",
      '30': "ERROR DE FORMATO",
      '39': "NO ES CUENTA DE CREDITO",
      '31': "BANCO NO SOPORTADO",
      '41': "TARJETA BLOQUEADA",
      '43': "TARJETA BLOQUEADA",
      '48': "CREDENCIAL INVALIDA",
      '50': "LLAMAR AL EMISOR",
      '51': "FONDOS INSUFICIENTES",
      '52': "NO ES CUENTA DE CHEQUES",
      '53': "NO ES CUENTA DE AHORROS",
      '54': "TARJETA EXPIRADA",
      '55': "PIN INCORRECTO",
      '56': "TARJETA NO VALIDA",
      '57': "TRANSACCION NO PERMITIDA",
      '58': "TRANSACCION NO PERMITIDA",
      '59': "SOSPECHA DE FRAUDE",
      '61': "ACTIVIDAD DE LIMITE EXCEDIDO",
      '62': "TARJETA RESTRINGIDA",
      '65': "MAXIMO PERMITIDO ALCANZADO",
      '75': "INTENTOS DE PIN EXCEDIDO",
      '82': "NO HSM",
      '83': "CUENTA NO EXISTE",
      '84': "CUENTA NO EXISTE",
      '85': "REGISTRO NO ENCONTRADO",
      '86': "AUTORIZACION NO VALIDA",
      '87': "CVV2 INVALIDO",
      '88': "ERROR EN LOG DE TRANSACCIONES",
      '89': "RUTA DE SERVICIO NO VALIDA",
      '91': "EMISOR NO DISPONIBLE",
      '92': "EMISOR NO DISPONIBLE",
      '93': "TRANSACCION NO PUEDE SER PROCESADA",
      '94': "TRANSACCION DUPLICADA",
      '96': "SISTEMA NO DISPONIBLE",
      '97': "TOKEN DE SEGURIDAD INVALIDO",
      'D0': "SISTEMA NO DISPONIBLE",
      'D1': "COMERCIO INVALIDO",
      'H0': "FOLIO YA EXISTE",
      'H1': "CHECK IN EXISTENTE",
      'H2': "SERVICIO DE RESERVACION NO PERMITIDO",
      'H3': "RESERVA NO ENCONTRADA EN EL SISTEMA",
      'H4': "TARJETA NO ENCONTRADA CHECK IN",
      'H5': "EXCEDE SOBREGIRO DE CHECK IN",
      'N0': "AUTORIZACION INHABILITADA",
      'N1': "TARJETA INVALIDA",
      'N2': "PREAUTORIZACIONES COMPLETAS",
      'N3': "MONTO MAXIMO ALCANZADO",
      'N4': "MONTO MAXIMO ALCANZADO",
      'N5': "MAXIMO DEVOLUCIONES ALCANZADO",
      'N6': "MAXIMO PERMITIDO ALCANZADO",
      'N7': "LLAMAR AL EMISOR",
      'N8': "CUENTA SOBREGIRADA",
      'N9': "INTENTOS PERMITIDOS ALCANZADO",
      'O0': "LLAMAR AL EMISOR",
      'O1': "NEG FILE PROBLEM",
      'O2': "MONTO DE RETIRO NO PERMITIDO",
      'O3': "DELINQUENT",
      'O4': "LIMITE EXCEDIDO",
      'O7': "FORCE POST",
      'O8': "SIN CUENTA",
      'O5': "PIN REQUERIDO",
      'O6': "DIGITO VERIFICADOR INVALIDO",
      'R8': "TARJETA BLOQUEADA",
      'T1': "MONTO INVALIDO",
      'T2': "FECHA DE TRANSACCION INVALIDA",
      'T5': "LLAMAR AL EMISOR",
    }
    return codes[code]
  },
  makeRequest
}