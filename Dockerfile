FROM node:latest
WORKDIR /usr/src/app
COPY ./package.json ./
COPY ./index.js ./
COPY ./tools.js ./
RUN npm i
CMD ["npm", "run", "start"]